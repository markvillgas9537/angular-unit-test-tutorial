import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Post } from 'src/app/models/Post';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent {
  @Input() post !: Post;
  @Output() delete = new EventEmitter<Post>();

  constructor() { }

  onDeletePost(event: Event) {
    event.preventDefault();
    this.delete.emit(this.post);
  }

}
