describe('First Test', () => {
    let testVariable: any;

    beforeEach(() => {
        testVariable = {};
    });

    it('Should return a true if a is a true', () => {
        // arrange
        testVariable.a = false;

        // act
        testVariable.a = true;
        
        // assert
        expect(testVariable.a).toBe(true);
    })
});