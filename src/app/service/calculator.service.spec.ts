import { CalculatorService } from "./calculator.service";
import { LoggerService } from "./logger.service";

describe('CalculatorService', () => {
  let mockLoggerService: any;
  let calculatorService: CalculatorService;

  beforeEach(() => {
    mockLoggerService = jasmine.createSpyObj('LoggerService', ['log']);
    calculatorService = new CalculatorService(mockLoggerService);
  });

  it('it can add numbers', () => {
    expect(calculatorService.add(2, 2)).toBe(4);
    expect(mockLoggerService.log).toHaveBeenCalledTimes(1);
  });

  it('it can subtract numbers', () => {
    expect(calculatorService.subtract(2, 2)).toBe(0);
    expect(mockLoggerService.log).toHaveBeenCalledTimes(1);
  });
});
