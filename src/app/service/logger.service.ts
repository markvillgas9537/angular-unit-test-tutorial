import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  public messages: string[] = [];

  constructor() { }

  log(message: any) {
    this.messages.push(message);
  }

  clear() {
    this.messages = [];
  }
}
