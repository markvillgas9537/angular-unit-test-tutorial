import { LoggerService } from "./logger.service";

describe('LoggerService', () => {
  let service: LoggerService;
  
  beforeEach(() => {
    // Arrange
    service = new LoggerService();
  });

  it('Should not have any message at starting', () => {
    // Act
    let count = service.messages.length;

    // Assert
    expect(count).toBe(0)
  });
  
  it('Should add messages when log is called', () => {
    // Act
    service.log('messages');

    // Assert
    expect(service.messages.length).toBe(1);
  });

  it('Should clear all messages when clear is called', () => {
    service.log('message');

    // Act
    service.clear();

    // Assert
    expect(service.messages.length).toBe(0);
  })
});