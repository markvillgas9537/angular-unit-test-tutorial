import { StrengthPipe } from './strength.pipe';

describe('StrengthPipe', () => {
  it('create an instance', () => {
    const pipe = new StrengthPipe();
    expect(pipe).toBeTruthy();
  });

  it('Should display weak if 5 is passed', () => {
    const pipe = new StrengthPipe();
    expect(pipe.transform(9)).toEqual('9 (weak)');
  }); 

  it('Should display strong if 10 is passed', () => {
    const pipe = new StrengthPipe();
    expect(pipe.transform(10)).toEqual('10 (strong)');
  });

  it('Should display strongest if 20 is passed', () => {
    const pipe = new StrengthPipe();
    expect(pipe.transform(20)).toEqual('20 (strongest)');
  });
});
